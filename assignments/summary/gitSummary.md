### **Git Summary**
***
**GitHub** is a code hosting platform for version control and collaboration. It lets you and others work together on projects from anywhere.
![branching.png](https://guides.github.com/activities/hello-world/branching.png)

**Git** and **GitHub** goes **hand-in-hand**
Workflow is as follows:  
1. A user writes and modifies code in his local system
    - There are 3 stages in this namely modified, staged, and committed
        - **Modified** means that you have changed the file but have not committed it to your repo yet.
        - **Staged** means that you have marked a modified file in its current version to go into your next picture/snapshot.
        - **Committed** means that the data is safely stored in your local repo in form of pictures/snapshots.
2. He then pushes the code block to the repository
3. Other users who want to work in his code can fork the repository and clone it to their git and work on the file(git pull)
4. After he is done with his work, he will push the file to the remote branch.
5. Use can open a pull request, When you open a pull request, you’re proposing your changes and requesting that someone review and pull in your contribution and merge them into their branch. Pull requests show diffs, or differences, of the content from both branches. The changes, additions, and subtractions are shown in green and red.
6. The work to a person is assigned in **issue** and clicking on **new issue**.It lists the works assigned to one,it also shows the number of open and closed issues for a user as shown in the image.
![](https://guides.github.com/features/issues/listing-screen.png)
7. When one is done with all the issues in the current branch, he requests a merge with the master branch.
#### Commands to perform various operations
***
- **Clone repo**:$ git clone (link to repository)
- **Create new branch**:$ git checkout master $ git checkout -b (your branch name)
- **Staging changes**:$ git add .
- **Commit changes**:$ git commit -sv
- **Push Files**:$ git push origin (branch name)
![](https://miro.medium.com/max/798/1*exCFWgo1cXpgCBmrwFRUUg.png)
